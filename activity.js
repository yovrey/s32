// 2. Create a simple server and the following routes with their corresponding HTTP methods and responses:

let http = require("http");
let port = 4000


http.createServer((request, response) => {

// a. If the url is http://localhost:4000/, send a response Welcome to Booking System

	if(request.url == "/" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to booking system")
	}



// b. If the url is http://localhost:4000/profile, send a response Welcome to your profile!

	if(request.url == "/profile" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Welcome to your profile")
	}



// c. If the url is http://localhost:4000/courses, send a response Here’s our courses available
	if(request.url == "/courses" && request.method == "GET") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Here's our courses available")
	}



// d. If the url is http://localhost:4000/addcourse, send a response Add a course to our resources

	if(request.url == "/addCourses" && request.method == "POST") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Add course to our resources")
	}



// e. If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources

	if(request.url == "/updateCourses" && request.method == "PUT") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Update a courses to our resources")
	}



// f. If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources

	if(request.url == "/archiveCourses" && request.method == "DELETE") {

		response.writeHead(200, {"Content-Type": "text/plain"})
		response.end("Archive courses to our resources")
	}

}).listen(port) 

console.log(`Server is running at localhost:${port}`)


